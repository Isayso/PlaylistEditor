﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using static PlaylistEditor.ClassDataset;

namespace PlaylistEditor
{
    internal class ClassPlayer
    {
        private static DataGridView dgv = new DataGridView();

        /// <summary>
        /// runs video player with link from current row
        /// </summary>
        /// <param name="dgv_imp"></param>
        public void RunPlayer(DataGridView dgv_imp)
        {
            dgv = dgv_imp;

            string param = GetLink2Play(dgv);

            //check whitch player and run it
            if (IsVlcPlayer)
                PlayOnVlc(param);

            else if (IsSmPlayer)
                PlayOnSMPlay(param);

            else if (IsFlyPlayer)
                PlayOnFly(param);

        }

        private string GetLink2Play (DataGridView dgv)
        {
            string playcell = dgv.CurrentRow.Cells["Link"].Value.ToString();
            string param = "";

            if (playcell.Contains("plugin") && playcell.Contains("youtube"))
            {

                string[] key = playcell.Split('=');  //variant normal or YT playlist link
                if (key.Length > 1)     //if link has no '='
                {
                    if (IsVlcPlayer)
                    {
                        //for (int i = 0; i < 3; i++)
                        //{
                        //    param = ClassHelp.GetVlcDashArg2(key[1]);  
                        //    if (param != "nodash" || param != "false")
                        //        break;
                        //}

                        int X = 0;
                        do
                        {
                            param = ClassHelp.GetVlcDashArg2(key[1]);

                            if (!param.Equals("false"))
                                break;
                            X++;

                        } while (X < 5);

                    }

                    else
                        param = "nodash";

                    if (param == "false")
                    {
                        NotificationBox.Show("Get HiRes Stream failed." + Environment.NewLine + "Try normal playback!", 4000, NotificationMsg.ERROR);
                        param = YTURL + key[1];

                    }
                    if (param == "nodash")
                    {
                        param = YTURL + key[1];
                    }
                }
            }
            else if (playcell.Contains("nfs"))
            {
                string nfs_ip = ClassImport.ScrapHtml(playcell, "\\b(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})\\b");
                if (ClassHelp.IsDriveReady(nfs_ip))
                {
                    //nfs: replace with file:///   
                    //replace / with \
                    playcell = playcell.Replace("/", "\\").Replace("nfs:", "file:///");

                    param = playcell.Quote();

                }
                else
                {
                    NotificationBox.Show("Drive not ready", 3000, NotificationMsg.ERROR);
                    return "0";
                }

            }
            else if (playcell.Contains(":\\") || playcell.Contains("\\\\"))
            {
                param = playcell.Quote();
            }
            else if (playcell.Contains("\\\\"))     //  \\nas
            {
                param = playcell.Quote();
            }
            else if (playcell.StartsWith("http"))     //  html option
            {
                param = " " + playcell;
            }
            else if (playcell.Contains("vimeo"))     //  html option
            {
                param = " " + ClassHelp.GetInetLink(VideoType.Vim, playcell);
            }
            else if (playcell.Contains("daily"))     //  html option
            {
                param = " " + ClassHelp.GetInetLink(VideoType.Daily, playcell);
            }


            return param;
        }

        private void PlayOnSMPlay(string param)
        {
           // string param = dgv.CurrentRow.Cells["Link"].Value.ToString();
            smppath = smppath + "\\";

            ProcessStartInfo ps = new ProcessStartInfo();
            ps.FileName = Path.Combine(smppath, "smplayer.exe");  //  ps.FileName = vlcpath + "\\" + "vlc.exe";

            ps.ErrorDialog = false;

            //no fullscreen
            //if (/*_isSingle && */Properties.Settings.Default.vlcFullscreen)
            //    ps.Arguments = " -minigui -fullscreen " + "\"" + param + "\"";

            //else if (_isSingle && !Settings.Default.vlc_fullsreen)
            //    ps.Arguments = " -minigui " + "\"" + param + "\"";//+ param;

           // else ps.Arguments = " -minigui " + param;
            ps.Arguments = " -minigui " + param;

            ps.CreateNoWindow = true;
            ps.UseShellExecute = false;

            ps.RedirectStandardOutput = true;
            ps.WindowStyle = ProcessWindowStyle.Hidden;

            using (Process proc = new Process())
            {
                proc.StartInfo = ps;

                proc.Start();
            }
        }
        private static void PlayOnVlc(string param)
        {
            string vlcpath = Properties.Settings.Default.vlcpath;
            ProcessStartInfo ps = new ProcessStartInfo();
            ps.FileName = vlcpath + "\\" + "vlc.exe";
            ps.ErrorDialog = false;
            ps.Arguments = param + " --no-video-title-show ";
            ps.Arguments += " --no-qt-error-dialogs";

            if (Properties.Settings.Default.vlc_autoclose)
                ps.Arguments += " vlc://quit";

            ps.CreateNoWindow = true;
            ps.UseShellExecute = false;

            ps.RedirectStandardOutput = true;
            ps.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

            using (Process proc = new Process())
            {
                proc.StartInfo = ps;

                proc.Start();
                //  proc.WaitForExit();

            }

            

        }

        private static void PlayOnFly(string flyArg)
        {

            string flypath = System.AppDomain.CurrentDomain.BaseDirectory + "player\\FlyleafPlayer.exe";
            string workpath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\PlaylistEditor";  //write to %AppData%
            System.IO.Directory.CreateDirectory(workpath);

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = flypath;
            startInfo.Arguments = flyArg;
            startInfo.RedirectStandardOutput = true;
            // startInfo.WorkingDirectory = System.AppDomain.CurrentDomain.BaseDirectory + "player";
            //startInfo.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);  //write to %AppData%
            startInfo.WorkingDirectory = workpath;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;

            using (Process proc = new Process())
            {
                proc.StartInfo = startInfo;

                proc.Start();
                //  proc.WaitForExit();
            }
        }



        private static bool IsVlcPlayer
        {
            get { return Properties.Settings.Default.vlcplayer; }
          //  get { return true; }
        }
        private static bool IsSmPlayer
        {
            get { return Properties.Settings.Default.smplayer; }
           // get { return false; }
        }
        private static bool IsFlyPlayer
        {
            get { return Properties.Settings.Default.flyplay; }
           // get { return false; }
        }



    }
}
