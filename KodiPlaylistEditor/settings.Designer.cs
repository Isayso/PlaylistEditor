﻿namespace PlaylistEditor
{
    partial class settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(settings));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checkBox_w = new System.Windows.Forms.CheckBox();
            this.textBox_hot = new System.Windows.Forms.TextBox();
            this.checkBox_a = new System.Windows.Forms.CheckBox();
            this.checkBox_s = new System.Windows.Forms.CheckBox();
            this.checkBox_c = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_Port = new System.Windows.Forms.TextBox();
            this.textBox_Username = new System.Windows.Forms.TextBox();
            this.textBox_Password = new System.Windows.Forms.TextBox();
            this.cbFlyleaf = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox_res = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox_kodi = new System.Windows.Forms.CheckBox();
            this.checkBox_w2 = new System.Windows.Forms.CheckBox();
            this.textBox_hot2 = new System.Windows.Forms.TextBox();
            this.checkBox_a2 = new System.Windows.Forms.CheckBox();
            this.checkBox_s2 = new System.Windows.Forms.CheckBox();
            this.checkBox_c2 = new System.Windows.Forms.CheckBox();
            this.tBQuery = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chbAutoCopy = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.btnAddpath = new System.Windows.Forms.Button();
            this.button_edit = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button_update = new System.Windows.Forms.Button();
            this.comboBox_download = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button_ok = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.cbSMPlayer = new System.Windows.Forms.CheckBox();
            this.cbVLCPlayer = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox_w
            // 
            this.checkBox_w.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_w.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_w.ForeColor = System.Drawing.Color.White;
            this.checkBox_w.Location = new System.Drawing.Point(368, 82);
            this.checkBox_w.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_w.Name = "checkBox_w";
            this.checkBox_w.Size = new System.Drawing.Size(80, 30);
            this.checkBox_w.TabIndex = 17;
            this.checkBox_w.Text = "WIN";
            this.toolTip1.SetToolTip(this.checkBox_w, "Restart to take effect");
            this.checkBox_w.UseVisualStyleBackColor = true;
            // 
            // textBox_hot
            // 
            this.textBox_hot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_hot.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_hot.Location = new System.Drawing.Point(471, 81);
            this.textBox_hot.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_hot.MaxLength = 1;
            this.textBox_hot.Name = "textBox_hot";
            this.textBox_hot.Size = new System.Drawing.Size(43, 28);
            this.textBox_hot.TabIndex = 16;
            this.textBox_hot.Text = "Y";
            this.textBox_hot.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.textBox_hot, "Restart to take effect");
            this.textBox_hot.Click += new System.EventHandler(this.textBox_hot_Click);
            // 
            // checkBox_a
            // 
            this.checkBox_a.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_a.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_a.ForeColor = System.Drawing.Color.White;
            this.checkBox_a.Location = new System.Drawing.Point(281, 82);
            this.checkBox_a.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_a.Name = "checkBox_a";
            this.checkBox_a.Size = new System.Drawing.Size(79, 30);
            this.checkBox_a.TabIndex = 14;
            this.checkBox_a.Text = "ALT";
            this.toolTip1.SetToolTip(this.checkBox_a, "Restart to take effect");
            this.checkBox_a.UseVisualStyleBackColor = true;
            // 
            // checkBox_s
            // 
            this.checkBox_s.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_s.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_s.ForeColor = System.Drawing.Color.White;
            this.checkBox_s.Location = new System.Drawing.Point(172, 82);
            this.checkBox_s.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_s.Name = "checkBox_s";
            this.checkBox_s.Size = new System.Drawing.Size(101, 30);
            this.checkBox_s.TabIndex = 13;
            this.checkBox_s.Text = "SHIFT";
            this.toolTip1.SetToolTip(this.checkBox_s, "Restart to take effect");
            this.checkBox_s.UseVisualStyleBackColor = true;
            // 
            // checkBox_c
            // 
            this.checkBox_c.Checked = true;
            this.checkBox_c.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_c.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_c.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_c.ForeColor = System.Drawing.Color.White;
            this.checkBox_c.Location = new System.Drawing.Point(56, 82);
            this.checkBox_c.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_c.Name = "checkBox_c";
            this.checkBox_c.Size = new System.Drawing.Size(96, 30);
            this.checkBox_c.TabIndex = 12;
            this.checkBox_c.Text = "CTRL";
            this.toolTip1.SetToolTip(this.checkBox_c, "Restart to take effect");
            this.checkBox_c.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(225, 76);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(221, 28);
            this.textBox2.TabIndex = 18;
            this.textBox2.Text = "192.168.178.91";
            this.toolTip1.SetToolTip(this.textBox2, "IP of nfs connected drive");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(87, 80);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 24);
            this.label3.TabIndex = 17;
            this.label3.Text = "Kodi IP";
            this.toolTip1.SetToolTip(this.label3, "nfs path on Kodi system");
            // 
            // checkBox2
            // 
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.ForeColor = System.Drawing.Color.White;
            this.checkBox2.Location = new System.Drawing.Point(23, 21);
            this.checkBox2.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(149, 30);
            this.checkBox2.TabIndex = 16;
            this.checkBox2.Text = "replace IPs";
            this.toolTip1.SetToolTip(this.checkBox2, "when add links");
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(197, 297);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(360, 28);
            this.textBox1.TabIndex = 15;
            this.textBox1.Text = "192.168.178.100";
            this.toolTip1.SetToolTip(this.textBox1, "IP of nfs connected drive");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(32, 300);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 24);
            this.label1.TabIndex = 14;
            this.label1.Text = "nfs path   nfs://";
            this.toolTip1.SetToolTip(this.label1, "nfs path on Kodi system");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(87, 127);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 24);
            this.label4.TabIndex = 19;
            this.label4.Text = "Port";
            this.toolTip1.SetToolTip(this.label4, "nfs path on Kodi system");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(87, 167);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 24);
            this.label5.TabIndex = 20;
            this.label5.Text = "Username";
            this.toolTip1.SetToolTip(this.label5, "nfs path on Kodi system");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(87, 215);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 24);
            this.label6.TabIndex = 21;
            this.label6.Text = "Password";
            this.toolTip1.SetToolTip(this.label6, "nfs path on Kodi system");
            // 
            // textBox_Port
            // 
            this.textBox_Port.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox_Port.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Port.Location = new System.Drawing.Point(225, 123);
            this.textBox_Port.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_Port.Name = "textBox_Port";
            this.textBox_Port.Size = new System.Drawing.Size(221, 28);
            this.textBox_Port.TabIndex = 22;
            this.textBox_Port.Text = "8080";
            this.toolTip1.SetToolTip(this.textBox_Port, "IP of nfs connected drive");
            // 
            // textBox_Username
            // 
            this.textBox_Username.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox_Username.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Username.Location = new System.Drawing.Point(225, 167);
            this.textBox_Username.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_Username.Name = "textBox_Username";
            this.textBox_Username.Size = new System.Drawing.Size(221, 28);
            this.textBox_Username.TabIndex = 23;
            this.textBox_Username.Text = "kodi";
            this.toolTip1.SetToolTip(this.textBox_Username, "IP of nfs connected drive");
            // 
            // textBox_Password
            // 
            this.textBox_Password.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox_Password.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Password.Location = new System.Drawing.Point(225, 212);
            this.textBox_Password.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_Password.Name = "textBox_Password";
            this.textBox_Password.PasswordChar = '*';
            this.textBox_Password.Size = new System.Drawing.Size(221, 28);
            this.textBox_Password.TabIndex = 24;
            this.toolTip1.SetToolTip(this.textBox_Password, "IP of nfs connected drive");
            // 
            // cbFlyleaf
            // 
            this.cbFlyleaf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbFlyleaf.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFlyleaf.ForeColor = System.Drawing.Color.White;
            this.cbFlyleaf.Location = new System.Drawing.Point(60, 21);
            this.cbFlyleaf.Margin = new System.Windows.Forms.Padding(4);
            this.cbFlyleaf.Name = "cbFlyleaf";
            this.cbFlyleaf.Size = new System.Drawing.Size(213, 30);
            this.cbFlyleaf.TabIndex = 17;
            this.cbFlyleaf.Text = "use Flyleaf player";
            this.toolTip1.SetToolTip(this.cbFlyleaf, "Flyleaf vs. VLC");
            this.cbFlyleaf.UseVisualStyleBackColor = true;
            this.cbFlyleaf.CheckedChanged += new System.EventHandler(this.cbFlyleaf_CheckedChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(56, 138);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(169, 24);
            this.label10.TabIndex = 18;
            this.label10.Text = "edit download path";
            this.toolTip1.SetToolTip(this.label10, "for video files");
            // 
            // comboBox_res
            // 
            this.comboBox_res.FormattingEnabled = true;
            this.comboBox_res.Items.AddRange(new object[] {
            "2160",
            "1440",
            "1080",
            "720",
            "480",
            "360"});
            this.comboBox_res.Location = new System.Drawing.Point(353, 80);
            this.comboBox_res.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox_res.Name = "comboBox_res";
            this.comboBox_res.Size = new System.Drawing.Size(148, 32);
            this.comboBox_res.TabIndex = 3;
            this.toolTip1.SetToolTip(this.comboBox_res, "vlc");
            this.comboBox_res.Click += new System.EventHandler(this.ComboBox_all_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(44, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(416, 24);
            this.label2.TabIndex = 15;
            this.label2.Text = "Get video link Hotkeys (restart after change)";
            this.toolTip1.SetToolTip(this.label2, "adds link from clipboard to editor");
            // 
            // checkBox_kodi
            // 
            this.checkBox_kodi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_kodi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_kodi.ForeColor = System.Drawing.Color.White;
            this.checkBox_kodi.Location = new System.Drawing.Point(56, 223);
            this.checkBox_kodi.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_kodi.Name = "checkBox_kodi";
            this.checkBox_kodi.Size = new System.Drawing.Size(488, 30);
            this.checkBox_kodi.TabIndex = 24;
            this.checkBox_kodi.Text = "Kodi direct play Hotkey (restart after change)";
            this.toolTip1.SetToolTip(this.checkBox_kodi, "when add links");
            this.checkBox_kodi.UseVisualStyleBackColor = true;
            this.checkBox_kodi.CheckedChanged += new System.EventHandler(this.checkBox_kodi_CheckedChanged);
            // 
            // checkBox_w2
            // 
            this.checkBox_w2.Checked = true;
            this.checkBox_w2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_w2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_w2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_w2.ForeColor = System.Drawing.Color.White;
            this.checkBox_w2.Location = new System.Drawing.Point(327, 23);
            this.checkBox_w2.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_w2.Name = "checkBox_w2";
            this.checkBox_w2.Size = new System.Drawing.Size(80, 30);
            this.checkBox_w2.TabIndex = 28;
            this.checkBox_w2.Text = "WIN";
            this.toolTip1.SetToolTip(this.checkBox_w2, "Restart to take effect");
            this.checkBox_w2.UseVisualStyleBackColor = true;
            // 
            // textBox_hot2
            // 
            this.textBox_hot2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_hot2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_hot2.Location = new System.Drawing.Point(429, 22);
            this.textBox_hot2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_hot2.MaxLength = 1;
            this.textBox_hot2.Name = "textBox_hot2";
            this.textBox_hot2.Size = new System.Drawing.Size(43, 28);
            this.textBox_hot2.TabIndex = 27;
            this.textBox_hot2.Text = "Y";
            this.textBox_hot2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.textBox_hot2, "Restart to take effect");
            // 
            // checkBox_a2
            // 
            this.checkBox_a2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_a2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_a2.ForeColor = System.Drawing.Color.White;
            this.checkBox_a2.Location = new System.Drawing.Point(240, 23);
            this.checkBox_a2.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_a2.Name = "checkBox_a2";
            this.checkBox_a2.Size = new System.Drawing.Size(79, 30);
            this.checkBox_a2.TabIndex = 26;
            this.checkBox_a2.Text = "ALT";
            this.toolTip1.SetToolTip(this.checkBox_a2, "Restart to take effect");
            this.checkBox_a2.UseVisualStyleBackColor = true;
            // 
            // checkBox_s2
            // 
            this.checkBox_s2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_s2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_s2.ForeColor = System.Drawing.Color.White;
            this.checkBox_s2.Location = new System.Drawing.Point(131, 23);
            this.checkBox_s2.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_s2.Name = "checkBox_s2";
            this.checkBox_s2.Size = new System.Drawing.Size(101, 30);
            this.checkBox_s2.TabIndex = 25;
            this.checkBox_s2.Text = "SHIFT";
            this.toolTip1.SetToolTip(this.checkBox_s2, "Restart to take effect");
            this.checkBox_s2.UseVisualStyleBackColor = true;
            // 
            // checkBox_c2
            // 
            this.checkBox_c2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_c2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_c2.ForeColor = System.Drawing.Color.White;
            this.checkBox_c2.Location = new System.Drawing.Point(15, 23);
            this.checkBox_c2.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_c2.Name = "checkBox_c2";
            this.checkBox_c2.Size = new System.Drawing.Size(96, 30);
            this.checkBox_c2.TabIndex = 24;
            this.checkBox_c2.Text = "CTRL";
            this.toolTip1.SetToolTip(this.checkBox_c2, "Restart to take effect");
            this.checkBox_c2.UseVisualStyleBackColor = true;
            // 
            // tBQuery
            // 
            this.tBQuery.BackColor = System.Drawing.SystemColors.Menu;
            this.tBQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tBQuery.Location = new System.Drawing.Point(67, 270);
            this.tBQuery.Margin = new System.Windows.Forms.Padding(4);
            this.tBQuery.Name = "tBQuery";
            this.tBQuery.Size = new System.Drawing.Size(501, 28);
            this.tBQuery.TabIndex = 17;
            this.tBQuery.Text = "https://www.google.com/search?q=";
            this.toolTip1.SetToolTip(this.tBQuery, "search strig for Google or else");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(61, 230);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 24);
            this.label11.TabIndex = 16;
            this.label11.Text = "Search query";
            this.toolTip1.SetToolTip(this.label11, "search strig for Google or else");
            // 
            // chbAutoCopy
            // 
            this.chbAutoCopy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chbAutoCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbAutoCopy.ForeColor = System.Drawing.Color.White;
            this.chbAutoCopy.Location = new System.Drawing.Point(56, 134);
            this.chbAutoCopy.Margin = new System.Windows.Forms.Padding(4);
            this.chbAutoCopy.Name = "chbAutoCopy";
            this.chbAutoCopy.Size = new System.Drawing.Size(488, 30);
            this.chbAutoCopy.TabIndex = 26;
            this.chbAutoCopy.Text = "Automatic copy";
            this.toolTip1.SetToolTip(this.chbAutoCopy, "sends Ctrl-c to active window");
            this.chbAutoCopy.UseVisualStyleBackColor = true;
            this.chbAutoCopy.CheckedChanged += new System.EventHandler(this.chbAutoCopy_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(49, 27);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(475, 24);
            this.label12.TabIndex = 16;
            this.label12.Text = "Plugin strings for supported Addons (need restart)";
            this.toolTip1.SetToolTip(this.label12, "adds link from clipboard to editor");
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(153, 79);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(501, 28);
            this.textBox3.TabIndex = 18;
            this.textBox3.Text = "plugin.video.youtube/play/?video_id=";
            this.toolTip1.SetToolTip(this.textBox3, "search strig for Google or else");
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(153, 119);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(501, 28);
            this.textBox4.TabIndex = 20;
            this.textBox4.Text = "plugin.video.rumble/?url=https://rumble.com/";
            this.toolTip1.SetToolTip(this.textBox4, "search strig for Google or else");
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(153, 160);
            this.textBox5.Margin = new System.Windows.Forms.Padding(4);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(501, 28);
            this.textBox5.TabIndex = 22;
            this.textBox5.Text = "plugin.video.lbry/play/";
            this.toolTip1.SetToolTip(this.textBox5, "search strig for Google or else");
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(153, 201);
            this.textBox6.Margin = new System.Windows.Forms.Padding(4);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(501, 28);
            this.textBox6.TabIndex = 24;
            this.textBox6.Text = "plugin.video.bitchute/play_now/";
            this.toolTip1.SetToolTip(this.textBox6, "search strig for Google or else");
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(153, 241);
            this.textBox7.Margin = new System.Windows.Forms.Padding(4);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(501, 28);
            this.textBox7.TabIndex = 26;
            this.textBox7.Text = "plugin.video.vimeo/play/?video_id=";
            this.toolTip1.SetToolTip(this.textBox7, "search strig for Google or else");
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.Menu;
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(153, 282);
            this.textBox8.Margin = new System.Windows.Forms.Padding(4);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(501, 28);
            this.textBox8.TabIndex = 28;
            this.textBox8.Text = "plugin.video.dailymotion_com/?url=";
            this.toolTip1.SetToolTip(this.textBox8, "search strig for Google or else");
            // 
            // btnAddpath
            // 
            this.btnAddpath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddpath.BackColor = System.Drawing.Color.MidnightBlue;
            this.btnAddpath.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddpath.BackgroundImage")));
            this.btnAddpath.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAddpath.FlatAppearance.BorderSize = 0;
            this.btnAddpath.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddpath.Location = new System.Drawing.Point(563, 167);
            this.btnAddpath.Margin = new System.Windows.Forms.Padding(0);
            this.btnAddpath.Name = "btnAddpath";
            this.btnAddpath.Size = new System.Drawing.Size(51, 46);
            this.btnAddpath.TabIndex = 70;
            this.toolTip1.SetToolTip(this.btnAddpath, "new download path");
            this.btnAddpath.UseVisualStyleBackColor = false;
            this.btnAddpath.Click += new System.EventHandler(this.btnAddpath_Click);
            // 
            // button_edit
            // 
            this.button_edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_edit.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_edit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_edit.FlatAppearance.BorderSize = 0;
            this.button_edit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_edit.Image = global::PlaylistEditor.Properties.Resources.close_circle;
            this.button_edit.Location = new System.Drawing.Point(624, 167);
            this.button_edit.Margin = new System.Windows.Forms.Padding(0);
            this.button_edit.Name = "button_edit";
            this.button_edit.Size = new System.Drawing.Size(53, 46);
            this.button_edit.TabIndex = 47;
            this.toolTip1.SetToolTip(this.button_edit, "delete entry");
            this.button_edit.UseVisualStyleBackColor = true;
            this.button_edit.Click += new System.EventHandler(this.button_edit_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 16);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(707, 438);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.DarkBlue;
            this.tabPage2.Controls.Add(this.textBox_Password);
            this.tabPage2.Controls.Add(this.textBox_Username);
            this.tabPage2.Controls.Add(this.textBox_Port);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.checkBox2);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.ForeColor = System.Drawing.Color.White;
            this.tabPage2.Location = new System.Drawing.Point(4, 33);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(699, 401);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Kodi";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.DarkBlue;
            this.tabPage4.Controls.Add(this.cbVLCPlayer);
            this.tabPage4.Controls.Add(this.cbSMPlayer);
            this.tabPage4.Controls.Add(this.btnAddpath);
            this.tabPage4.Controls.Add(this.button_update);
            this.tabPage4.Controls.Add(this.comboBox_download);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.cbFlyleaf);
            this.tabPage4.Controls.Add(this.comboBox_res);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.button_edit);
            this.tabPage4.ForeColor = System.Drawing.Color.White;
            this.tabPage4.Location = new System.Drawing.Point(4, 33);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(699, 401);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "video";
            // 
            // button_update
            // 
            this.button_update.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.163636F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_update.ForeColor = System.Drawing.Color.White;
            this.button_update.Location = new System.Drawing.Point(251, 305);
            this.button_update.Margin = new System.Windows.Forms.Padding(4);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(239, 46);
            this.button_update.TabIndex = 15;
            this.button_update.Text = "Update youtube-dl";
            this.button_update.UseVisualStyleBackColor = true;
            this.button_update.Click += new System.EventHandler(this.Button_update_Click);
            // 
            // comboBox_download
            // 
            this.comboBox_download.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_download.BackColor = System.Drawing.Color.MidnightBlue;
            this.comboBox_download.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_download.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_download.ForeColor = System.Drawing.Color.White;
            this.comboBox_download.FormattingEnabled = true;
            this.comboBox_download.ItemHeight = 24;
            this.comboBox_download.Items.AddRange(new object[] {
            "new path"});
            this.comboBox_download.Location = new System.Drawing.Point(135, 174);
            this.comboBox_download.Margin = new System.Windows.Forms.Padding(0);
            this.comboBox_download.Name = "comboBox_download";
            this.comboBox_download.Size = new System.Drawing.Size(404, 32);
            this.comboBox_download.TabIndex = 46;
            this.comboBox_download.Click += new System.EventHandler(this.ComboBox_all_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(56, 84);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(203, 24);
            this.label9.TabIndex = 1;
            this.label9.Text = "default video resolution";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DarkBlue;
            this.tabPage1.Controls.Add(this.chbAutoCopy);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.checkBox_kodi);
            this.tabPage1.Controls.Add(this.checkBox_w);
            this.tabPage1.Controls.Add(this.textBox_hot);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.checkBox_a);
            this.tabPage1.Controls.Add(this.checkBox_s);
            this.tabPage1.Controls.Add(this.checkBox_c);
            this.tabPage1.ForeColor = System.Drawing.Color.White;
            this.tabPage1.Location = new System.Drawing.Point(4, 33);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(699, 401);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "HotKeys";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBox_w2);
            this.panel1.Controls.Add(this.textBox_hot2);
            this.panel1.Controls.Add(this.checkBox_a2);
            this.panel1.Controls.Add(this.checkBox_s2);
            this.panel1.Controls.Add(this.checkBox_c2);
            this.panel1.Location = new System.Drawing.Point(41, 260);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(488, 78);
            this.panel1.TabIndex = 25;
            this.panel1.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.DarkBlue;
            this.tabPage3.Controls.Add(this.tBQuery);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.comboBox2);
            this.tabPage3.Controls.Add(this.comboBox1);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.ForeColor = System.Drawing.Color.White;
            this.tabPage3.Location = new System.Drawing.Point(4, 33);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(699, 401);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "search";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Name",
            "Link"});
            this.comboBox2.Location = new System.Drawing.Point(404, 134);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(105, 32);
            this.comboBox2.TabIndex = 3;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            this.comboBox2.Click += new System.EventHandler(this.ComboBox_all_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Name",
            "Link"});
            this.comboBox1.Location = new System.Drawing.Point(404, 62);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(107, 32);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.Click += new System.EventHandler(this.ComboBox_all_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(61, 138);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(217, 24);
            this.label8.TabIndex = 1;
            this.label8.Text = "Colum search duplicates";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(61, 65);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 24);
            this.label7.TabIndex = 0;
            this.label7.Text = "Colum search";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.DarkBlue;
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.textBox8);
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Controls.Add(this.textBox7);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.textBox6);
            this.tabPage5.Controls.Add(this.label15);
            this.tabPage5.Controls.Add(this.textBox5);
            this.tabPage5.Controls.Add(this.label14);
            this.tabPage5.Controls.Add(this.textBox4);
            this.tabPage5.Controls.Add(this.label13);
            this.tabPage5.Controls.Add(this.textBox3);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Location = new System.Drawing.Point(4, 33);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage5.Size = new System.Drawing.Size(699, 401);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Addons";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(15, 286);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(107, 24);
            this.label18.TabIndex = 29;
            this.label18.Text = "Dailymotion";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(15, 245);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 24);
            this.label17.TabIndex = 27;
            this.label17.Text = "Vimeo";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(15, 204);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 24);
            this.label16.TabIndex = 25;
            this.label16.Text = "Bitchute";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(15, 164);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 24);
            this.label15.TabIndex = 23;
            this.label15.Text = "Odysee";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(15, 123);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 24);
            this.label14.TabIndex = 21;
            this.label14.Text = "Rumble";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(15, 82);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 24);
            this.label13.TabIndex = 19;
            this.label13.Text = "Youtube";
            // 
            // button_ok
            // 
            this.button_ok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_ok.FlatAppearance.BorderSize = 0;
            this.button_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ok.Image = ((System.Drawing.Image)(resources.GetObject("button_ok.Image")));
            this.button_ok.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_ok.Location = new System.Drawing.Point(376, 474);
            this.button_ok.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_ok.Name = "button_ok";
            this.button_ok.Size = new System.Drawing.Size(141, 53);
            this.button_ok.TabIndex = 18;
            this.button_ok.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ok.UseVisualStyleBackColor = true;
            this.button_ok.Click += new System.EventHandler(this.button_ok_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_cancel.FlatAppearance.BorderSize = 0;
            this.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_cancel.Image = ((System.Drawing.Image)(resources.GetObject("button_cancel.Image")));
            this.button_cancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button_cancel.Location = new System.Drawing.Point(171, 474);
            this.button_cancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(136, 53);
            this.button_cancel.TabIndex = 17;
            this.button_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // cbSMPlayer
            // 
            this.cbSMPlayer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbSMPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSMPlayer.ForeColor = System.Drawing.Color.White;
            this.cbSMPlayer.Location = new System.Drawing.Point(251, 21);
            this.cbSMPlayer.Margin = new System.Windows.Forms.Padding(4);
            this.cbSMPlayer.Name = "cbSMPlayer";
            this.cbSMPlayer.Size = new System.Drawing.Size(213, 30);
            this.cbSMPlayer.TabIndex = 71;
            this.cbSMPlayer.Text = "use SM player";
            this.cbSMPlayer.UseVisualStyleBackColor = true;
            this.cbSMPlayer.CheckedChanged += new System.EventHandler(this.cbSMPlayer_CheckedChanged);
            // 
            // cbVLCPlayer
            // 
            this.cbVLCPlayer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbVLCPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVLCPlayer.ForeColor = System.Drawing.Color.White;
            this.cbVLCPlayer.Location = new System.Drawing.Point(447, 21);
            this.cbVLCPlayer.Margin = new System.Windows.Forms.Padding(4);
            this.cbVLCPlayer.Name = "cbVLCPlayer";
            this.cbVLCPlayer.Size = new System.Drawing.Size(213, 30);
            this.cbVLCPlayer.TabIndex = 72;
            this.cbVLCPlayer.Text = "use VLC player";
            this.cbVLCPlayer.UseVisualStyleBackColor = true;
            this.cbVLCPlayer.CheckedChanged += new System.EventHandler(this.cbVLCPlayer_CheckedChanged);
            // 
            // settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(707, 553);
            this.Controls.Add(this.button_cancel);
            this.Controls.Add(this.button_ok);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "settings";
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox checkBox_w;
        private System.Windows.Forms.TextBox textBox_hot;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox_a;
        private System.Windows.Forms.CheckBox checkBox_s;
        private System.Windows.Forms.CheckBox checkBox_c;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Password;
        private System.Windows.Forms.TextBox textBox_Username;
        private System.Windows.Forms.TextBox textBox_Port;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox comboBox_res;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cbFlyleaf;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button_edit;
        public System.Windows.Forms.ComboBox comboBox_download;
        private System.Windows.Forms.Button button_update;
        private System.Windows.Forms.CheckBox checkBox_kodi;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBox_w2;
        private System.Windows.Forms.TextBox textBox_hot2;
        private System.Windows.Forms.CheckBox checkBox_a2;
        private System.Windows.Forms.CheckBox checkBox_s2;
        private System.Windows.Forms.CheckBox checkBox_c2;
        private System.Windows.Forms.TextBox tBQuery;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnAddpath;
        private System.Windows.Forms.CheckBox chbAutoCopy;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Button button_ok;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.CheckBox cbVLCPlayer;
        private System.Windows.Forms.CheckBox cbSMPlayer;
    }
}