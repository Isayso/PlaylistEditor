﻿namespace PlaylistEditor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cms1KodiPlay = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1KodiQueue = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1OpenLink = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cms1Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1PasteAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1Cut = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cms1Send2Clip = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1Search = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1ExportList = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cms1Download = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1Rename = new System.Windows.Forms.ToolStripMenuItem();
            this.cms1NewWIndow = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.checkBox_unix = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox_download = new System.Windows.Forms.ComboBox();
            this.contextMenuStrip4 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmsDeletePathItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBox_video = new System.Windows.Forms.ComboBox();
            this.comboBox_audio = new System.Windows.Forms.ComboBox();
            this.button_download_start = new System.Windows.Forms.Button();
            this.button_path = new System.Windows.Forms.Button();
            this.button_cancel = new System.Windows.Forms.Button();
            this.RedoButton = new System.Windows.Forms.Button();
            this.UndoButton = new System.Windows.Forms.Button();
            this.btn_CheckInet = new System.Windows.Forms.Button();
            this.button_vlc = new System.Windows.Forms.Button();
            this.cmLabelVlc = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.cmLbl480 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmLbl720 = new System.Windows.Forms.ToolStripMenuItem();
            this.cmLbl1080 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cmLblAutoclose = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_TagInternetLink = new System.Windows.Forms.Button();
            this.button_download = new System.Windows.Forms.Button();
            this.button_dup = new System.Windows.Forms.Button();
            this.cm5_Dup = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cms5SearchCombo = new System.Windows.Forms.ToolStripComboBox();
            this.cms5SearchDupli = new System.Windows.Forms.ToolStripMenuItem();
            this.button_search = new System.Windows.Forms.Button();
            this.button_del_all = new System.Windows.Forms.Button();
            this.button_settings = new System.Windows.Forms.Button();
            this.button_add = new System.Windows.Forms.Button();
            this.button_Info = new System.Windows.Forms.Button();
            this.button_delLine = new System.Windows.Forms.Button();
            this.button_save = new System.Windows.Forms.Button();
            this.button_open = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label_counter = new System.Windows.Forms.Label();
            this.label_progress = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.checkBox_rlink = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label_central = new System.Windows.Forms.Label();
            this.contextMenuStrip3 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnSelectList = new System.Windows.Forms.Button();
            this.checkBox_replaceIP = new System.Windows.Forms.CheckBox();
            this.btnShuffleData = new RepeatingButton();
            this.btnReverseRows = new RepeatingButton();
            this.buttonR_MoveDown = new RepeatingButton();
            this.buttonR_moveUp = new RepeatingButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel_Find = new System.Windows.Forms.Panel();
            this.button_refind = new System.Windows.Forms.Button();
            this.button_clearfind = new System.Windows.Forms.Button();
            this.textBox_find = new System.Windows.Forms.TextBox();
            this.plabel_Filename = new PathLabel();
            this.lblNoRows = new System.Windows.Forms.Label();
            this.button_revert = new System.Windows.Forms.Button();
            this.cm7_Open = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cm7Open = new System.Windows.Forms.ToolStripMenuItem();
            this.cm7Append = new System.Windows.Forms.ToolStripMenuItem();
            this.cm7NewWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.cm7Label1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cm7Label2 = new System.Windows.Forms.ToolStripMenuItem();
            this.cm7Label3 = new System.Windows.Forms.ToolStripMenuItem();
            this.cm7Label4 = new System.Windows.Forms.ToolStripMenuItem();
            this.cm7Label5 = new System.Windows.Forms.ToolStripMenuItem();
            this.cm6_Save = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cm6Save = new System.Windows.Forms.ToolStripMenuItem();
            this.cm6Saveas = new System.Windows.Forms.ToolStripMenuItem();
            this.plabel_Filename2 = new PathLabel();
            this.cms5FileExt = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip4.SuspendLayout();
            this.cmLabelVlc.SuspendLayout();
            this.cm5_Dup.SuspendLayout();
            this.panel1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.contextMenuStrip3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel_Find.SuspendLayout();
            this.cm7_Open.SuspendLayout();
            this.cm6_Save.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.163636F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.Location = new System.Drawing.Point(-4, 69);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 47;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1440, 360);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDoubleClick);
            this.dataGridView1.CellMouseEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseEnter);
            this.dataGridView1.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellMouseLeave);
            this.dataGridView1.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dataGridView1_CellPainting);
            this.dataGridView1.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellValidated);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            this.dataGridView1.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView1_RowsAdded);
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            this.dataGridView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragDrop);
            this.dataGridView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragEnter);
            this.dataGridView1.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragOver);
            this.dataGridView1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDown);
            this.dataGridView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseMove);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cms1KodiPlay,
            this.cms1KodiQueue,
            this.cms1OpenLink,
            this.toolStripSeparator1,
            this.cms1Copy,
            this.cms1PasteAdd,
            this.cms1Cut,
            this.toolStripSeparator2,
            this.cms1Send2Clip,
            this.cms1Search,
            this.cms1ExportList,
            this.toolStripSeparator3,
            this.cms1Download,
            this.cms1Rename,
            this.cms1NewWIndow});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(345, 382);
            this.contextMenuStrip1.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.contextMenuStrip1_Closing);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // cms1KodiPlay
            // 
            this.cms1KodiPlay.Image = ((System.Drawing.Image)(resources.GetObject("cms1KodiPlay.Image")));
            this.cms1KodiPlay.Name = "cms1KodiPlay";
            this.cms1KodiPlay.ShortcutKeyDisplayString = "Ctrl+P";
            this.cms1KodiPlay.Size = new System.Drawing.Size(344, 30);
            this.cms1KodiPlay.Text = "Kodi play";
            this.cms1KodiPlay.Click += new System.EventHandler(this.cms1KodiPlay_Click);
            // 
            // cms1KodiQueue
            // 
            this.cms1KodiQueue.Image = ((System.Drawing.Image)(resources.GetObject("cms1KodiQueue.Image")));
            this.cms1KodiQueue.Name = "cms1KodiQueue";
            this.cms1KodiQueue.ShortcutKeyDisplayString = "Ctrl+Q";
            this.cms1KodiQueue.Size = new System.Drawing.Size(344, 30);
            this.cms1KodiQueue.Text = "Kodi queue";
            this.cms1KodiQueue.Click += new System.EventHandler(this.cms1KodiQueue_Click);
            // 
            // cms1OpenLink
            // 
            this.cms1OpenLink.Name = "cms1OpenLink";
            this.cms1OpenLink.ShortcutKeyDisplayString = "Ctrl+L";
            this.cms1OpenLink.Size = new System.Drawing.Size(344, 30);
            this.cms1OpenLink.Text = "Open video location";
            this.cms1OpenLink.Click += new System.EventHandler(this.cms1OpenLink_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(341, 6);
            // 
            // cms1Copy
            // 
            this.cms1Copy.Image = ((System.Drawing.Image)(resources.GetObject("cms1Copy.Image")));
            this.cms1Copy.Name = "cms1Copy";
            this.cms1Copy.ShortcutKeyDisplayString = "Ctrl+C";
            this.cms1Copy.Size = new System.Drawing.Size(344, 30);
            this.cms1Copy.Text = "Copy row";
            this.cms1Copy.Click += new System.EventHandler(this.cms1Copy_Click);
            // 
            // cms1PasteAdd
            // 
            this.cms1PasteAdd.Image = ((System.Drawing.Image)(resources.GetObject("cms1PasteAdd.Image")));
            this.cms1PasteAdd.Name = "cms1PasteAdd";
            this.cms1PasteAdd.ShortcutKeyDisplayString = "Ctrl+V";
            this.cms1PasteAdd.Size = new System.Drawing.Size(344, 30);
            this.cms1PasteAdd.Text = "Paste Insert";
            this.cms1PasteAdd.Click += new System.EventHandler(this.cms1PasteAdd_Click);
            // 
            // cms1Cut
            // 
            this.cms1Cut.Image = ((System.Drawing.Image)(resources.GetObject("cms1Cut.Image")));
            this.cms1Cut.Name = "cms1Cut";
            this.cms1Cut.ShortcutKeyDisplayString = "Ctrl+X";
            this.cms1Cut.Size = new System.Drawing.Size(344, 30);
            this.cms1Cut.Text = "Cut row";
            this.cms1Cut.Click += new System.EventHandler(this.cms1Cut_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(341, 6);
            // 
            // cms1Send2Clip
            // 
            this.cms1Send2Clip.Name = "cms1Send2Clip";
            this.cms1Send2Clip.ShortcutKeyDisplayString = "Alt+C";
            this.cms1Send2Clip.Size = new System.Drawing.Size(344, 30);
            this.cms1Send2Clip.Text = "Send Link to Clipboard";
            this.cms1Send2Clip.ToolTipText = "Loop to send multiple selected links to Clipboard.\r\nCan be used to transfer links" +
    " to external downloader\r\nlike JDownload.";
            this.cms1Send2Clip.Click += new System.EventHandler(this.cms1Send2Clipb);
            // 
            // cms1Search
            // 
            this.cms1Search.Name = "cms1Search";
            this.cms1Search.ShortcutKeyDisplayString = "Ctrl+G";
            this.cms1Search.Size = new System.Drawing.Size(344, 30);
            this.cms1Search.Text = "Search Name with iNet";
            this.cms1Search.Click += new System.EventHandler(this.SearchGoogletoolStriptem_Click);
            // 
            // cms1ExportList
            // 
            this.cms1ExportList.Name = "cms1ExportList";
            this.cms1ExportList.Size = new System.Drawing.Size(344, 30);
            this.cms1ExportList.Text = "Export List";
            this.cms1ExportList.Click += new System.EventHandler(this.cms1ExportList_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(341, 6);
            // 
            // cms1Download
            // 
            this.cms1Download.Name = "cms1Download";
            this.cms1Download.ShortcutKeyDisplayString = "";
            this.cms1Download.Size = new System.Drawing.Size(344, 30);
            this.cms1Download.Text = "Download YT video";
            this.cms1Download.Click += new System.EventHandler(this.btn_downloadYTFile_Click);
            // 
            // cms1Rename
            // 
            this.cms1Rename.Image = ((System.Drawing.Image)(resources.GetObject("cms1Rename.Image")));
            this.cms1Rename.Name = "cms1Rename";
            this.cms1Rename.ShortcutKeyDisplayString = "F2";
            this.cms1Rename.Size = new System.Drawing.Size(344, 30);
            this.cms1Rename.Text = "Rename";
            this.cms1Rename.Click += new System.EventHandler(this.editF2ToolStripMenuItem_Click);
            // 
            // cms1NewWIndow
            // 
            this.cms1NewWIndow.Name = "cms1NewWIndow";
            this.cms1NewWIndow.ShortcutKeyDisplayString = "Ctrl+N";
            this.cms1NewWIndow.Size = new System.Drawing.Size(344, 30);
            this.cms1NewWIndow.Text = "New Window";
            this.cms1NewWIndow.Click += new System.EventHandler(this.cms1NewWIndow_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "m3u";
            this.openFileDialog.Filter = "Playlist|*.m3u";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "m3u";
            this.saveFileDialog1.Filter = "Playlist|*.m3u";
            // 
            // checkBox_unix
            // 
            this.checkBox_unix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_unix.Checked = true;
            this.checkBox_unix.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_unix.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_unix.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_unix.ForeColor = System.Drawing.Color.White;
            this.checkBox_unix.Location = new System.Drawing.Point(1036, 4);
            this.checkBox_unix.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_unix.Name = "checkBox_unix";
            this.checkBox_unix.Size = new System.Drawing.Size(97, 30);
            this.checkBox_unix.TabIndex = 30;
            this.checkBox_unix.Text = "Linux";
            this.toolTip1.SetToolTip(this.checkBox_unix, "playlist for Linux Kodi systems");
            this.checkBox_unix.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.BackColor = System.Drawing.Color.MidnightBlue;
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.854546F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.ForeColor = System.Drawing.Color.White;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.ItemHeight = 16;
            this.comboBox1.Items.AddRange(new object[] {
            "2160",
            "1440",
            "1080",
            "720",
            "480",
            "360"});
            this.comboBox1.Location = new System.Drawing.Point(274, 89);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(84, 24);
            this.comboBox1.TabIndex = 41;
            this.toolTip1.SetToolTip(this.comboBox1, "resolution for play and download");
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.Click += new System.EventHandler(this.ComboBox_Click);
            // 
            // comboBox_download
            // 
            this.comboBox_download.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_download.BackColor = System.Drawing.Color.MidnightBlue;
            this.comboBox_download.ContextMenuStrip = this.contextMenuStrip4;
            this.comboBox_download.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_download.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_download.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_download.ForeColor = System.Drawing.Color.White;
            this.comboBox_download.FormattingEnabled = true;
            this.comboBox_download.ItemHeight = 24;
            this.comboBox_download.Items.AddRange(new object[] {
            "new path"});
            this.comboBox_download.Location = new System.Drawing.Point(19, 14);
            this.comboBox_download.Margin = new System.Windows.Forms.Padding(0);
            this.comboBox_download.Name = "comboBox_download";
            this.comboBox_download.Size = new System.Drawing.Size(437, 32);
            this.comboBox_download.TabIndex = 43;
            this.toolTip1.SetToolTip(this.comboBox_download, "download path");
            this.comboBox_download.Click += new System.EventHandler(this.ComboBox_Click);
            // 
            // contextMenuStrip4
            // 
            this.contextMenuStrip4.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmsDeletePathItem});
            this.contextMenuStrip4.Name = "contextMenuStrip4";
            this.contextMenuStrip4.ShowImageMargin = false;
            this.contextMenuStrip4.Size = new System.Drawing.Size(132, 28);
            // 
            // cmsDeletePathItem
            // 
            this.cmsDeletePathItem.Name = "cmsDeletePathItem";
            this.cmsDeletePathItem.Size = new System.Drawing.Size(131, 24);
            this.cmsDeletePathItem.Text = "Delete path";
            this.cmsDeletePathItem.Click += new System.EventHandler(this.cmsDeletePathItem_Click);
            // 
            // comboBox_video
            // 
            this.comboBox_video.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_video.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_video.FormattingEnabled = true;
            this.comboBox_video.Items.AddRange(new object[] {
            "mp4",
            "webm",
            "audio"});
            this.comboBox_video.Location = new System.Drawing.Point(19, 86);
            this.comboBox_video.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox_video.Name = "comboBox_video";
            this.comboBox_video.Size = new System.Drawing.Size(116, 32);
            this.comboBox_video.TabIndex = 53;
            this.comboBox_video.Click += new System.EventHandler(this.ComboBox_Click);
            // 
            // comboBox_audio
            // 
            this.comboBox_audio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox_audio.Enabled = false;
            this.comboBox_audio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_audio.FormattingEnabled = true;
            this.comboBox_audio.Items.AddRange(new object[] {
            "auto",
            "m4a",
            "aac",
            "ogg"});
            this.comboBox_audio.Location = new System.Drawing.Point(164, 86);
            this.comboBox_audio.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox_audio.Name = "comboBox_audio";
            this.comboBox_audio.Size = new System.Drawing.Size(90, 32);
            this.comboBox_audio.TabIndex = 54;
            this.comboBox_audio.Click += new System.EventHandler(this.ComboBox_Click);
            // 
            // button_download_start
            // 
            this.button_download_start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_download_start.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_download_start.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_download_start.FlatAppearance.BorderSize = 0;
            this.button_download_start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_download_start.Image = ((System.Drawing.Image)(resources.GetObject("button_download_start.Image")));
            this.button_download_start.Location = new System.Drawing.Point(297, 146);
            this.button_download_start.Margin = new System.Windows.Forms.Padding(0);
            this.button_download_start.Name = "button_download_start";
            this.button_download_start.Size = new System.Drawing.Size(143, 57);
            this.button_download_start.TabIndex = 65;
            this.button_download_start.UseVisualStyleBackColor = true;
            this.button_download_start.Click += new System.EventHandler(this.button_download_start_Click);
            // 
            // button_path
            // 
            this.button_path.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_path.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_path.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_path.FlatAppearance.BorderSize = 0;
            this.button_path.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_path.Image = ((System.Drawing.Image)(resources.GetObject("button_path.Image")));
            this.button_path.Location = new System.Drawing.Point(475, 9);
            this.button_path.Margin = new System.Windows.Forms.Padding(0);
            this.button_path.Name = "button_path";
            this.button_path.Size = new System.Drawing.Size(51, 46);
            this.button_path.TabIndex = 69;
            this.button_path.UseVisualStyleBackColor = false;
            this.button_path.Click += new System.EventHandler(this.button_path_Click);
            // 
            // button_cancel
            // 
            this.button_cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_cancel.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_cancel.FlatAppearance.BorderSize = 0;
            this.button_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_cancel.Image = ((System.Drawing.Image)(resources.GetObject("button_cancel.Image")));
            this.button_cancel.Location = new System.Drawing.Point(129, 146);
            this.button_cancel.Margin = new System.Windows.Forms.Padding(0);
            this.button_cancel.Name = "button_cancel";
            this.button_cancel.Size = new System.Drawing.Size(152, 57);
            this.button_cancel.TabIndex = 59;
            this.button_cancel.UseVisualStyleBackColor = true;
            this.button_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // RedoButton
            // 
            this.RedoButton.BackColor = System.Drawing.Color.MidnightBlue;
            this.RedoButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.RedoButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.RedoButton.FlatAppearance.BorderSize = 0;
            this.RedoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RedoButton.Image = ((System.Drawing.Image)(resources.GetObject("RedoButton.Image")));
            this.RedoButton.Location = new System.Drawing.Point(144, 39);
            this.RedoButton.Margin = new System.Windows.Forms.Padding(0);
            this.RedoButton.Name = "RedoButton";
            this.RedoButton.Size = new System.Drawing.Size(44, 23);
            this.RedoButton.TabIndex = 62;
            this.toolTip1.SetToolTip(this.RedoButton, "redo");
            this.RedoButton.UseVisualStyleBackColor = true;
            this.RedoButton.Click += new System.EventHandler(this.RedoButton_Click);
            // 
            // UndoButton
            // 
            this.UndoButton.BackColor = System.Drawing.Color.MidnightBlue;
            this.UndoButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.UndoButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.UndoButton.FlatAppearance.BorderSize = 0;
            this.UndoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UndoButton.Image = ((System.Drawing.Image)(resources.GetObject("UndoButton.Image")));
            this.UndoButton.Location = new System.Drawing.Point(144, 5);
            this.UndoButton.Margin = new System.Windows.Forms.Padding(0);
            this.UndoButton.Name = "UndoButton";
            this.UndoButton.Size = new System.Drawing.Size(44, 23);
            this.UndoButton.TabIndex = 61;
            this.toolTip1.SetToolTip(this.UndoButton, "undo");
            this.UndoButton.UseVisualStyleBackColor = true;
            this.UndoButton.Click += new System.EventHandler(this.UndoButton_Click);
            // 
            // btn_CheckInet
            // 
            this.btn_CheckInet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_CheckInet.BackColor = System.Drawing.Color.MidnightBlue;
            this.btn_CheckInet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_CheckInet.FlatAppearance.BorderSize = 0;
            this.btn_CheckInet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_CheckInet.Image = ((System.Drawing.Image)(resources.GetObject("btn_CheckInet.Image")));
            this.btn_CheckInet.Location = new System.Drawing.Point(1195, 20);
            this.btn_CheckInet.Margin = new System.Windows.Forms.Padding(0);
            this.btn_CheckInet.Name = "btn_CheckInet";
            this.btn_CheckInet.Size = new System.Drawing.Size(33, 44);
            this.btn_CheckInet.TabIndex = 60;
            this.toolTip1.SetToolTip(this.btn_CheckInet, "check for valid links\r\n+CTRL select links");
            this.btn_CheckInet.UseVisualStyleBackColor = true;
            this.btn_CheckInet.Click += new System.EventHandler(this.btn_CheckInet_Click);
            // 
            // button_vlc
            // 
            this.button_vlc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_vlc.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_vlc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_vlc.ContextMenuStrip = this.cmLabelVlc;
            this.button_vlc.FlatAppearance.BorderSize = 0;
            this.button_vlc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_vlc.Image = ((System.Drawing.Image)(resources.GetObject("button_vlc.Image")));
            this.button_vlc.Location = new System.Drawing.Point(901, 12);
            this.button_vlc.Margin = new System.Windows.Forms.Padding(0);
            this.button_vlc.Name = "button_vlc";
            this.button_vlc.Size = new System.Drawing.Size(53, 46);
            this.button_vlc.TabIndex = 38;
            this.toolTip1.SetToolTip(this.button_vlc, "play with ");
            this.button_vlc.UseVisualStyleBackColor = true;
            this.button_vlc.Click += new System.EventHandler(this.button_vlc_Click);
            // 
            // cmLabelVlc
            // 
            this.cmLabelVlc.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmLabelVlc.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.cmLabelVlc.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripSeparator5,
            this.cmLbl480,
            this.cmLbl720,
            this.cmLbl1080,
            this.toolStripSeparator4,
            this.cmLblAutoclose});
            this.cmLabelVlc.Name = "cmLabelVlc";
            this.cmLabelVlc.ShowCheckMargin = true;
            this.cmLabelVlc.ShowImageMargin = false;
            this.cmLabelVlc.Size = new System.Drawing.Size(204, 166);
            this.cmLabelVlc.Opening += new System.ComponentModel.CancelEventHandler(this.cmLabelVlc_Opening);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Enabled = false;
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.Black;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(203, 30);
            this.toolStripMenuItem1.Text = "VLC Options";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(200, 6);
            // 
            // cmLbl480
            // 
            this.cmLbl480.CheckOnClick = true;
            this.cmLbl480.Name = "cmLbl480";
            this.cmLbl480.Size = new System.Drawing.Size(203, 30);
            this.cmLbl480.Text = "480";
            this.cmLbl480.Click += new System.EventHandler(this.cmLbl_click);
            // 
            // cmLbl720
            // 
            this.cmLbl720.CheckOnClick = true;
            this.cmLbl720.Name = "cmLbl720";
            this.cmLbl720.Size = new System.Drawing.Size(203, 30);
            this.cmLbl720.Text = "720";
            this.cmLbl720.Click += new System.EventHandler(this.cmLbl_click);
            // 
            // cmLbl1080
            // 
            this.cmLbl1080.CheckOnClick = true;
            this.cmLbl1080.Name = "cmLbl1080";
            this.cmLbl1080.Size = new System.Drawing.Size(203, 30);
            this.cmLbl1080.Text = "1080";
            this.cmLbl1080.Click += new System.EventHandler(this.cmLbl_click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(200, 6);
            // 
            // cmLblAutoclose
            // 
            this.cmLblAutoclose.CheckOnClick = true;
            this.cmLblAutoclose.Name = "cmLblAutoclose";
            this.cmLblAutoclose.Size = new System.Drawing.Size(203, 30);
            this.cmLblAutoclose.Text = "VLC Autoclose";
            this.cmLblAutoclose.Click += new System.EventHandler(this.cmLbl_click);
            // 
            // btn_TagInternetLink
            // 
            this.btn_TagInternetLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_TagInternetLink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_TagInternetLink.FlatAppearance.BorderSize = 0;
            this.btn_TagInternetLink.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TagInternetLink.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_TagInternetLink.Image = ((System.Drawing.Image)(resources.GetObject("btn_TagInternetLink.Image")));
            this.btn_TagInternetLink.Location = new System.Drawing.Point(1137, 20);
            this.btn_TagInternetLink.Margin = new System.Windows.Forms.Padding(0);
            this.btn_TagInternetLink.Name = "btn_TagInternetLink";
            this.btn_TagInternetLink.Size = new System.Drawing.Size(51, 42);
            this.btn_TagInternetLink.TabIndex = 36;
            this.toolTip1.SetToolTip(this.btn_TagInternetLink, "select internet links");
            this.btn_TagInternetLink.UseVisualStyleBackColor = true;
            this.btn_TagInternetLink.Click += new System.EventHandler(this.btn_TagInternetLink_Click);
            // 
            // button_download
            // 
            this.button_download.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_download.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_download.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_download.FlatAppearance.BorderSize = 0;
            this.button_download.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_download.Image = ((System.Drawing.Image)(resources.GetObject("button_download.Image")));
            this.button_download.Location = new System.Drawing.Point(971, 14);
            this.button_download.Margin = new System.Windows.Forms.Padding(0);
            this.button_download.Name = "button_download";
            this.button_download.Size = new System.Drawing.Size(49, 46);
            this.button_download.TabIndex = 40;
            this.toolTip1.SetToolTip(this.button_download, "download YT video");
            this.button_download.UseVisualStyleBackColor = true;
            this.button_download.Click += new System.EventHandler(this.btn_downloadYTFile_Click);
            // 
            // button_dup
            // 
            this.button_dup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_dup.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_dup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_dup.ContextMenuStrip = this.cm5_Dup;
            this.button_dup.FlatAppearance.BorderSize = 0;
            this.button_dup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_dup.Image = ((System.Drawing.Image)(resources.GetObject("button_dup.Image")));
            this.button_dup.Location = new System.Drawing.Point(1243, 18);
            this.button_dup.Margin = new System.Windows.Forms.Padding(0);
            this.button_dup.Name = "button_dup";
            this.button_dup.Size = new System.Drawing.Size(33, 46);
            this.button_dup.TabIndex = 35;
            this.toolTip1.SetToolTip(this.button_dup, "find duplicates\r\n+SHIFT delete\r\nright click: choose column");
            this.button_dup.UseVisualStyleBackColor = true;
            this.button_dup.Click += new System.EventHandler(this.button_dup_Click);
            // 
            // cm5_Dup
            // 
            this.cm5_Dup.BackColor = System.Drawing.Color.MidnightBlue;
            this.cm5_Dup.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm5_Dup.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.cm5_Dup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cms5SearchCombo,
            this.cms5FileExt,
            this.cms5SearchDupli});
            this.cm5_Dup.Name = "contextMenuStrip5";
            this.cm5_Dup.ShowCheckMargin = true;
            this.cm5_Dup.ShowImageMargin = false;
            this.cm5_Dup.Size = new System.Drawing.Size(256, 136);
            this.cm5_Dup.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.cm5_Dup_Closing);
            this.cm5_Dup.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip5_Opening);
            this.cm5_Dup.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cm5_Dup_MouseDown);
            // 
            // cms5SearchCombo
            // 
            this.cms5SearchCombo.BackColor = System.Drawing.SystemColors.Control;
            this.cms5SearchCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cms5SearchCombo.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cms5SearchCombo.Items.AddRange(new object[] {
            "Name",
            "Link"});
            this.cms5SearchCombo.Name = "cms5SearchCombo";
            this.cms5SearchCombo.Size = new System.Drawing.Size(121, 36);
            this.cms5SearchCombo.SelectedIndexChanged += new System.EventHandler(this.cms5SearchCombo_SelectedIndexChanged);
            // 
            // cms5SearchDupli
            // 
            this.cms5SearchDupli.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cms5SearchDupli.ForeColor = System.Drawing.SystemColors.Control;
            this.cms5SearchDupli.Name = "cms5SearchDupli";
            this.cms5SearchDupli.Size = new System.Drawing.Size(255, 32);
            this.cms5SearchDupli.Text = "Search Duplicates";
            this.cms5SearchDupli.Click += new System.EventHandler(this.cms5SearchDupli_Click);
            // 
            // button_search
            // 
            this.button_search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_search.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_search.FlatAppearance.BorderSize = 0;
            this.button_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_search.Image = ((System.Drawing.Image)(resources.GetObject("button_search.Image")));
            this.button_search.Location = new System.Drawing.Point(1288, 17);
            this.button_search.Margin = new System.Windows.Forms.Padding(0);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(33, 46);
            this.button_search.TabIndex = 33;
            this.toolTip1.SetToolTip(this.button_search, "search\r\nCTRL+F");
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.button_search_Click);
            // 
            // button_del_all
            // 
            this.button_del_all.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_del_all.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_del_all.FlatAppearance.BorderSize = 0;
            this.button_del_all.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_del_all.Image = ((System.Drawing.Image)(resources.GetObject("button_del_all.Image")));
            this.button_del_all.Location = new System.Drawing.Point(391, 12);
            this.button_del_all.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_del_all.Name = "button_del_all";
            this.button_del_all.Size = new System.Drawing.Size(40, 39);
            this.button_del_all.TabIndex = 29;
            this.button_del_all.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.button_del_all, "delete all");
            this.button_del_all.UseVisualStyleBackColor = true;
            this.button_del_all.Click += new System.EventHandler(this.button_del_all_Click);
            // 
            // button_settings
            // 
            this.button_settings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_settings.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_settings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_settings.FlatAppearance.BorderSize = 0;
            this.button_settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_settings.Image = ((System.Drawing.Image)(resources.GetObject("button_settings.Image")));
            this.button_settings.Location = new System.Drawing.Point(1335, 18);
            this.button_settings.Margin = new System.Windows.Forms.Padding(0);
            this.button_settings.Name = "button_settings";
            this.button_settings.Size = new System.Drawing.Size(33, 46);
            this.button_settings.TabIndex = 28;
            this.toolTip1.SetToolTip(this.button_settings, "settings");
            this.button_settings.UseVisualStyleBackColor = true;
            this.button_settings.Click += new System.EventHandler(this.button_settings_Click);
            // 
            // button_add
            // 
            this.button_add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_add.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_add.FlatAppearance.BorderSize = 0;
            this.button_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_add.Image = ((System.Drawing.Image)(resources.GetObject("button_add.Image")));
            this.button_add.Location = new System.Drawing.Point(341, 14);
            this.button_add.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(40, 39);
            this.button_add.TabIndex = 27;
            this.button_add.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.button_add, "new row");
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_Info
            // 
            this.button_Info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Info.BackColor = System.Drawing.Color.MidnightBlue;
            this.button_Info.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_Info.FlatAppearance.BorderSize = 0;
            this.button_Info.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Info.Image = ((System.Drawing.Image)(resources.GetObject("button_Info.Image")));
            this.button_Info.Location = new System.Drawing.Point(1385, 18);
            this.button_Info.Margin = new System.Windows.Forms.Padding(0);
            this.button_Info.Name = "button_Info";
            this.button_Info.Size = new System.Drawing.Size(33, 46);
            this.button_Info.TabIndex = 24;
            this.toolTip1.SetToolTip(this.button_Info, "info/help");
            this.button_Info.UseVisualStyleBackColor = true;
            this.button_Info.Click += new System.EventHandler(this.button_Info_Click);
            // 
            // button_delLine
            // 
            this.button_delLine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_delLine.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_delLine.FlatAppearance.BorderSize = 0;
            this.button_delLine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_delLine.Image = ((System.Drawing.Image)(resources.GetObject("button_delLine.Image")));
            this.button_delLine.Location = new System.Drawing.Point(195, 14);
            this.button_delLine.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_delLine.Name = "button_delLine";
            this.button_delLine.Size = new System.Drawing.Size(40, 39);
            this.button_delLine.TabIndex = 2;
            this.button_delLine.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.button_delLine, "delete row");
            this.button_delLine.UseVisualStyleBackColor = true;
            this.button_delLine.Click += new System.EventHandler(this.button_delLine_Click);
            // 
            // button_save
            // 
            this.button_save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_save.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_save.FlatAppearance.BorderSize = 0;
            this.button_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_save.Image = ((System.Drawing.Image)(resources.GetObject("button_save.Image")));
            this.button_save.Location = new System.Drawing.Point(77, 2);
            this.button_save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(60, 60);
            this.button_save.TabIndex = 1;
            this.button_save.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.button_save, "save as");
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // button_open
            // 
            this.button_open.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_open.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_open.FlatAppearance.BorderSize = 0;
            this.button_open.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_open.Image = ((System.Drawing.Image)(resources.GetObject("button_open.Image")));
            this.button_open.Location = new System.Drawing.Point(12, 2);
            this.button_open.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_open.Name = "button_open";
            this.button_open.Size = new System.Drawing.Size(60, 60);
            this.button_open.TabIndex = 0;
            this.button_open.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.button_open, "open m3u file");
            this.button_open.UseVisualStyleBackColor = true;
            this.button_open.Click += new System.EventHandler(this.button_open_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.label_counter);
            this.panel1.Controls.Add(this.label_progress);
            this.panel1.Controls.Add(this.lbl8);
            this.panel1.Controls.Add(this.lbl7);
            this.panel1.Controls.Add(this.button_download_start);
            this.panel1.Controls.Add(this.button_path);
            this.panel1.Controls.Add(this.checkBox_rlink);
            this.panel1.Controls.Add(this.comboBox_audio);
            this.panel1.Controls.Add(this.button_cancel);
            this.panel1.Controls.Add(this.comboBox_video);
            this.panel1.Controls.Add(this.comboBox_download);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Location = new System.Drawing.Point(897, 70);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(536, 208);
            this.panel1.TabIndex = 44;
            this.panel1.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(19, 175);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(100, 25);
            this.progressBar1.TabIndex = 78;
            this.progressBar1.Visible = false;
            // 
            // label_counter
            // 
            this.label_counter.AutoSize = true;
            this.label_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_counter.ForeColor = System.Drawing.Color.White;
            this.label_counter.Location = new System.Drawing.Point(27, 146);
            this.label_counter.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_counter.Name = "label_counter";
            this.label_counter.Size = new System.Drawing.Size(45, 24);
            this.label_counter.TabIndex = 73;
            this.label_counter.Text = "1 / 2";
            this.label_counter.Visible = false;
            // 
            // label_progress
            // 
            this.label_progress.AutoSize = true;
            this.label_progress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.78182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_progress.ForeColor = System.Drawing.SystemColors.Control;
            this.label_progress.Location = new System.Drawing.Point(27, 172);
            this.label_progress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_progress.Name = "label_progress";
            this.label_progress.Size = new System.Drawing.Size(0, 25);
            this.label_progress.TabIndex = 72;
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8.ForeColor = System.Drawing.Color.White;
            this.lbl8.Location = new System.Drawing.Point(167, 58);
            this.lbl8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(57, 24);
            this.lbl8.TabIndex = 71;
            this.lbl8.Text = "audio";
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7.ForeColor = System.Drawing.Color.White;
            this.lbl7.Location = new System.Drawing.Point(24, 58);
            this.lbl7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(56, 24);
            this.lbl7.TabIndex = 70;
            this.lbl7.Text = "video";
            // 
            // checkBox_rlink
            // 
            this.checkBox_rlink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_rlink.Checked = true;
            this.checkBox_rlink.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_rlink.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_rlink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_rlink.ForeColor = System.Drawing.Color.White;
            this.checkBox_rlink.Location = new System.Drawing.Point(384, 86);
            this.checkBox_rlink.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_rlink.Name = "checkBox_rlink";
            this.checkBox_rlink.Size = new System.Drawing.Size(137, 30);
            this.checkBox_rlink.TabIndex = 58;
            this.checkBox_rlink.Text = "replace link";
            this.checkBox_rlink.UseVisualStyleBackColor = true;
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.cutToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(190, 94);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeyDisplayString = "Crtl-C";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(189, 30);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.editCellCopy_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl-V";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(189, 30);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.editCellPaste_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl-X";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(189, 30);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.editCellCut_Click);
            // 
            // label_central
            // 
            this.label_central.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_central.AutoSize = true;
            this.label_central.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label_central.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.818182F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_central.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_central.Location = new System.Drawing.Point(560, 195);
            this.label_central.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_central.Name = "label_central";
            this.label_central.Size = new System.Drawing.Size(215, 60);
            this.label_central.TabIndex = 64;
            this.label_central.Text = "Double Click to open file\r\nDrag \'n Drop video files\r\nCTRL-N Open new Window";
            this.label_central.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label_central.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            // 
            // contextMenuStrip3
            // 
            this.contextMenuStrip3.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.contextMenuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteEntryToolStripMenuItem});
            this.contextMenuStrip3.Name = "contextMenuStrip3";
            this.contextMenuStrip3.ShowImageMargin = false;
            this.contextMenuStrip3.Size = new System.Drawing.Size(135, 28);
            // 
            // deleteEntryToolStripMenuItem
            // 
            this.deleteEntryToolStripMenuItem.Name = "deleteEntryToolStripMenuItem";
            this.deleteEntryToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
            this.deleteEntryToolStripMenuItem.Text = "Delete entry";
            this.deleteEntryToolStripMenuItem.Click += new System.EventHandler(this.deleteEntryToolStripMenuItem_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            // 
            // btnSelectList
            // 
            this.btnSelectList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSelectList.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSelectList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSelectList.FlatAppearance.BorderSize = 0;
            this.btnSelectList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectList.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectList.Image")));
            this.btnSelectList.Location = new System.Drawing.Point(741, 4);
            this.btnSelectList.Margin = new System.Windows.Forms.Padding(0);
            this.btnSelectList.Name = "btnSelectList";
            this.btnSelectList.Size = new System.Drawing.Size(34, 34);
            this.btnSelectList.TabIndex = 78;
            this.toolTip1.SetToolTip(this.btnSelectList, "switch duplicate lists");
            this.btnSelectList.UseVisualStyleBackColor = false;
            this.btnSelectList.Click += new System.EventHandler(this.btnSelectList_Click);
            // 
            // checkBox_replaceIP
            // 
            this.checkBox_replaceIP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_replaceIP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_replaceIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_replaceIP.ForeColor = System.Drawing.Color.White;
            this.checkBox_replaceIP.Location = new System.Drawing.Point(1036, 34);
            this.checkBox_replaceIP.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_replaceIP.Name = "checkBox_replaceIP";
            this.checkBox_replaceIP.Size = new System.Drawing.Size(97, 30);
            this.checkBox_replaceIP.TabIndex = 72;
            this.checkBox_replaceIP.Text = "rep. IP";
            this.toolTip1.SetToolTip(this.checkBox_replaceIP, "replace WIndows Drives with IP");
            this.checkBox_replaceIP.UseVisualStyleBackColor = true;
            this.checkBox_replaceIP.CheckedChanged += new System.EventHandler(this.checkBox_replaceIP_CheckedChanged);
            // 
            // btnShuffleData
            // 
            this.btnShuffleData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnShuffleData.FlatAppearance.BorderSize = 0;
            this.btnShuffleData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShuffleData.Image = ((System.Drawing.Image)(resources.GetObject("btnShuffleData.Image")));
            this.btnShuffleData.Location = new System.Drawing.Point(284, 5);
            this.btnShuffleData.Margin = new System.Windows.Forms.Padding(0);
            this.btnShuffleData.Name = "btnShuffleData";
            this.btnShuffleData.Size = new System.Drawing.Size(40, 25);
            this.btnShuffleData.TabIndex = 70;
            this.btnShuffleData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnShuffleData, "shuffle rows");
            this.btnShuffleData.UseVisualStyleBackColor = true;
            this.btnShuffleData.Click += new System.EventHandler(this.btnShuffleData_Click);
            // 
            // btnReverseRows
            // 
            this.btnReverseRows.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnReverseRows.FlatAppearance.BorderSize = 0;
            this.btnReverseRows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReverseRows.Image = ((System.Drawing.Image)(resources.GetObject("btnReverseRows.Image")));
            this.btnReverseRows.Location = new System.Drawing.Point(283, 39);
            this.btnReverseRows.Margin = new System.Windows.Forms.Padding(0);
            this.btnReverseRows.Name = "btnReverseRows";
            this.btnReverseRows.Size = new System.Drawing.Size(40, 25);
            this.btnReverseRows.TabIndex = 69;
            this.btnReverseRows.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.btnReverseRows, "reverse rows");
            this.btnReverseRows.UseVisualStyleBackColor = true;
            this.btnReverseRows.Click += new System.EventHandler(this.btnReverseRows_Click);
            // 
            // buttonR_MoveDown
            // 
            this.buttonR_MoveDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonR_MoveDown.FlatAppearance.BorderSize = 0;
            this.buttonR_MoveDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonR_MoveDown.Image = ((System.Drawing.Image)(resources.GetObject("buttonR_MoveDown.Image")));
            this.buttonR_MoveDown.Location = new System.Drawing.Point(238, 41);
            this.buttonR_MoveDown.Margin = new System.Windows.Forms.Padding(0);
            this.buttonR_MoveDown.Name = "buttonR_MoveDown";
            this.buttonR_MoveDown.Size = new System.Drawing.Size(40, 25);
            this.buttonR_MoveDown.TabIndex = 32;
            this.buttonR_MoveDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.buttonR_MoveDown, "move row down\r\nCTRL+2\r\n");
            this.buttonR_MoveDown.UseVisualStyleBackColor = true;
            this.buttonR_MoveDown.Click += new System.EventHandler(this.button_moveDown_Click);
            // 
            // buttonR_moveUp
            // 
            this.buttonR_moveUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonR_moveUp.FlatAppearance.BorderSize = 0;
            this.buttonR_moveUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonR_moveUp.Image = ((System.Drawing.Image)(resources.GetObject("buttonR_moveUp.Image")));
            this.buttonR_moveUp.Location = new System.Drawing.Point(238, 6);
            this.buttonR_moveUp.Margin = new System.Windows.Forms.Padding(0);
            this.buttonR_moveUp.Name = "buttonR_moveUp";
            this.buttonR_moveUp.Size = new System.Drawing.Size(40, 25);
            this.buttonR_moveUp.TabIndex = 31;
            this.buttonR_moveUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.toolTip1.SetToolTip(this.buttonR_moveUp, "move row up\r\nCTRL+1");
            this.buttonR_moveUp.UseVisualStyleBackColor = true;
            this.buttonR_moveUp.Click += new System.EventHandler(this.button_moveUp_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btnSelectList, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel_Find, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.plabel_Filename, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNoRows, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_revert, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 430);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1432, 43);
            this.tableLayoutPanel1.TabIndex = 71;
            // 
            // panel_Find
            // 
            this.panel_Find.Controls.Add(this.button_refind);
            this.panel_Find.Controls.Add(this.button_clearfind);
            this.panel_Find.Controls.Add(this.textBox_find);
            this.panel_Find.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel_Find.Location = new System.Drawing.Point(1049, 3);
            this.panel_Find.Name = "panel_Find";
            this.panel_Find.Size = new System.Drawing.Size(380, 37);
            this.panel_Find.TabIndex = 77;
            this.panel_Find.Visible = false;
            // 
            // button_refind
            // 
            this.button_refind.BackColor = System.Drawing.Color.White;
            this.button_refind.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_refind.Image = ((System.Drawing.Image)(resources.GetObject("button_refind.Image")));
            this.button_refind.Location = new System.Drawing.Point(340, 1);
            this.button_refind.Name = "button_refind";
            this.button_refind.Size = new System.Drawing.Size(37, 32);
            this.button_refind.TabIndex = 36;
            this.button_refind.UseVisualStyleBackColor = false;
            this.button_refind.Click += new System.EventHandler(this.button_refind_Click);
            // 
            // button_clearfind
            // 
            this.button_clearfind.BackColor = System.Drawing.Color.White;
            this.button_clearfind.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_clearfind.Image = ((System.Drawing.Image)(resources.GetObject("button_clearfind.Image")));
            this.button_clearfind.Location = new System.Drawing.Point(302, 1);
            this.button_clearfind.Name = "button_clearfind";
            this.button_clearfind.Size = new System.Drawing.Size(37, 32);
            this.button_clearfind.TabIndex = 35;
            this.button_clearfind.UseVisualStyleBackColor = false;
            this.button_clearfind.Click += new System.EventHandler(this.button_clearfind_Click);
            // 
            // textBox_find
            // 
            this.textBox_find.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox_find.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.74545F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_find.Location = new System.Drawing.Point(0, 1);
            this.textBox_find.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_find.Name = "textBox_find";
            this.textBox_find.Size = new System.Drawing.Size(300, 33);
            this.textBox_find.TabIndex = 34;
            this.textBox_find.TextChanged += new System.EventHandler(this.textBox_find_TextChanged);
            // 
            // plabel_Filename
            // 
            this.plabel_Filename.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.plabel_Filename.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.78182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plabel_Filename.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.plabel_Filename.Location = new System.Drawing.Point(39, 7);
            this.plabel_Filename.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.plabel_Filename.Name = "plabel_Filename";
            this.plabel_Filename.Size = new System.Drawing.Size(600, 28);
            this.plabel_Filename.TabIndex = 76;
            this.plabel_Filename.Text = "pathLabel1";
            this.plabel_Filename.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblNoRows
            // 
            this.lblNoRows.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNoRows.AutoSize = true;
            this.lblNoRows.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F);
            this.lblNoRows.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblNoRows.Location = new System.Drawing.Point(694, 10);
            this.lblNoRows.Name = "lblNoRows";
            this.lblNoRows.Size = new System.Drawing.Size(28, 22);
            this.lblNoRows.TabIndex = 75;
            this.lblNoRows.Text = "xx";
            // 
            // button_revert
            // 
            this.button_revert.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_revert.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button_revert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button_revert.FlatAppearance.BorderSize = 0;
            this.button_revert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_revert.Image = ((System.Drawing.Image)(resources.GetObject("button_revert.Image")));
            this.button_revert.Location = new System.Drawing.Point(1, 5);
            this.button_revert.Margin = new System.Windows.Forms.Padding(0);
            this.button_revert.Name = "button_revert";
            this.button_revert.Size = new System.Drawing.Size(33, 33);
            this.button_revert.TabIndex = 38;
            this.button_revert.UseVisualStyleBackColor = false;
            this.button_revert.Visible = false;
            this.button_revert.Click += new System.EventHandler(this.button_revert_Click);
            // 
            // cm7_Open
            // 
            this.cm7_Open.BackColor = System.Drawing.Color.MidnightBlue;
            this.cm7_Open.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm7_Open.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cm7_Open.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm7Open,
            this.cm7Append,
            this.cm7NewWindow,
            this.toolStripSeparator6,
            this.cm7Label1,
            this.cm7Label2,
            this.cm7Label3,
            this.cm7Label4,
            this.cm7Label5});
            this.cm7_Open.Name = "cm7_Open";
            this.cm7_Open.ShowImageMargin = false;
            this.cm7_Open.Size = new System.Drawing.Size(255, 266);
            this.cm7_Open.Opening += new System.ComponentModel.CancelEventHandler(this.cm7_Open_Opening);
            // 
            // cm7Open
            // 
            this.cm7Open.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm7Open.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7Open.Name = "cm7Open";
            this.cm7Open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.cm7Open.Size = new System.Drawing.Size(254, 32);
            this.cm7Open.Text = "open";
            this.cm7Open.Click += new System.EventHandler(this.label_open_Click);
            // 
            // cm7Append
            // 
            this.cm7Append.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm7Append.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7Append.Name = "cm7Append";
            this.cm7Append.Size = new System.Drawing.Size(254, 32);
            this.cm7Append.Text = "append";
            this.cm7Append.Click += new System.EventHandler(this.cm7Append_Click);
            // 
            // cm7NewWindow
            // 
            this.cm7NewWindow.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7NewWindow.Name = "cm7NewWindow";
            this.cm7NewWindow.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.cm7NewWindow.Size = new System.Drawing.Size(254, 32);
            this.cm7NewWindow.Text = "new window";
            this.cm7NewWindow.Click += new System.EventHandler(this.cm7NewWindow_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(251, 6);
            // 
            // cm7Label1
            // 
            this.cm7Label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm7Label1.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7Label1.Name = "cm7Label1";
            this.cm7Label1.Size = new System.Drawing.Size(254, 32);
            this.cm7Label1.Text = "Label1";
            this.cm7Label1.Click += new System.EventHandler(this.labelMRU_Click);
            // 
            // cm7Label2
            // 
            this.cm7Label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm7Label2.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7Label2.Name = "cm7Label2";
            this.cm7Label2.Size = new System.Drawing.Size(254, 32);
            this.cm7Label2.Text = "Label2";
            this.cm7Label2.Click += new System.EventHandler(this.labelMRU_Click);
            // 
            // cm7Label3
            // 
            this.cm7Label3.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm7Label3.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7Label3.Name = "cm7Label3";
            this.cm7Label3.Size = new System.Drawing.Size(254, 32);
            this.cm7Label3.Text = "Label3";
            this.cm7Label3.Click += new System.EventHandler(this.labelMRU_Click);
            // 
            // cm7Label4
            // 
            this.cm7Label4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm7Label4.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7Label4.Name = "cm7Label4";
            this.cm7Label4.Size = new System.Drawing.Size(254, 32);
            this.cm7Label4.Text = "Label4";
            this.cm7Label4.Click += new System.EventHandler(this.labelMRU_Click);
            // 
            // cm7Label5
            // 
            this.cm7Label5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm7Label5.ForeColor = System.Drawing.SystemColors.Control;
            this.cm7Label5.Name = "cm7Label5";
            this.cm7Label5.Size = new System.Drawing.Size(254, 32);
            this.cm7Label5.Text = "Label5";
            this.cm7Label5.Click += new System.EventHandler(this.labelMRU_Click);
            // 
            // cm6_Save
            // 
            this.cm6_Save.BackColor = System.Drawing.Color.MidnightBlue;
            this.cm6_Save.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cm6_Save.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cm6_Save.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cm6Save,
            this.cm6Saveas});
            this.cm6_Save.Name = "cm6_Save";
            this.cm6_Save.ShowImageMargin = false;
            this.cm6_Save.Size = new System.Drawing.Size(173, 68);
            // 
            // cm6Save
            // 
            this.cm6Save.ForeColor = System.Drawing.SystemColors.Control;
            this.cm6Save.Name = "cm6Save";
            this.cm6Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.cm6Save.Size = new System.Drawing.Size(172, 32);
            this.cm6Save.Text = "save";
            this.cm6Save.Click += new System.EventHandler(this.cm6Save_Click);
            // 
            // cm6Saveas
            // 
            this.cm6Saveas.ForeColor = System.Drawing.SystemColors.Control;
            this.cm6Saveas.Name = "cm6Saveas";
            this.cm6Saveas.Size = new System.Drawing.Size(172, 32);
            this.cm6Saveas.Text = "save as";
            this.cm6Saveas.Click += new System.EventHandler(this.cm6Saveas_Click);
            // 
            // plabel_Filename2
            // 
            this.plabel_Filename2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.plabel_Filename2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plabel_Filename2.ForeColor = System.Drawing.Color.Cyan;
            this.plabel_Filename2.Location = new System.Drawing.Point(7, 449);
            this.plabel_Filename2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.plabel_Filename2.Name = "plabel_Filename2";
            this.plabel_Filename2.Size = new System.Drawing.Size(620, 20);
            this.plabel_Filename2.TabIndex = 67;
            // 
            // cms5FileExt
            // 
            this.cms5FileExt.CheckOnClick = true;
            this.cms5FileExt.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.cms5FileExt.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cms5FileExt.Name = "cms5FileExt";
            this.cms5FileExt.Size = new System.Drawing.Size(255, 32);
            this.cms5FileExt.Text = "Ignore File Extensions";
            this.cms5FileExt.CheckedChanged += new System.EventHandler(this.cms5FileExt_CheckedChanged);
            this.cms5FileExt.Click += new System.EventHandler(this.cms5FileExt_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(1432, 473);
            this.Controls.Add(this.checkBox_replaceIP);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btnShuffleData);
            this.Controls.Add(this.btnReverseRows);
            this.Controls.Add(this.plabel_Filename2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label_central);
            this.Controls.Add(this.RedoButton);
            this.Controls.Add(this.UndoButton);
            this.Controls.Add(this.btn_CheckInet);
            this.Controls.Add(this.button_vlc);
            this.Controls.Add(this.btn_TagInternetLink);
            this.Controls.Add(this.button_download);
            this.Controls.Add(this.button_dup);
            this.Controls.Add(this.button_search);
            this.Controls.Add(this.buttonR_MoveDown);
            this.Controls.Add(this.buttonR_moveUp);
            this.Controls.Add(this.checkBox_unix);
            this.Controls.Add(this.button_del_all);
            this.Controls.Add(this.button_settings);
            this.Controls.Add(this.button_add);
            this.Controls.Add(this.button_Info);
            this.Controls.Add(this.button_delLine);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.button_open);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Playlist Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip4.ResumeLayout(false);
            this.cmLabelVlc.ResumeLayout(false);
            this.cm5_Dup.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.contextMenuStrip3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel_Find.ResumeLayout(false);
            this.panel_Find.PerformLayout();
            this.cm7_Open.ResumeLayout(false);
            this.cm6_Save.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_open;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Button button_delLine;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button_Info;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button button_settings;
        private System.Windows.Forms.Button button_del_all;
        private System.Windows.Forms.CheckBox checkBox_unix;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cms1Copy;
        private System.Windows.Forms.ToolStripMenuItem cms1PasteAdd;
        private System.Windows.Forms.ToolStripMenuItem cms1Send2Clip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private RepeatingButton buttonR_moveUp;
        private RepeatingButton buttonR_MoveDown;
        private System.Windows.Forms.Button button_search;
        private System.Windows.Forms.Button button_dup;
        private System.Windows.Forms.Button btn_TagInternetLink;
        private System.Windows.Forms.ToolStripMenuItem cms1Cut;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Button button_vlc;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cms1Download;
        private System.Windows.Forms.Button button_download;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        public System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolStripMenuItem cms1OpenLink;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.ComboBox comboBox_download;
        private System.Windows.Forms.ComboBox comboBox_video;
        private System.Windows.Forms.ComboBox comboBox_audio;
        private System.Windows.Forms.CheckBox checkBox_rlink;
        private System.Windows.Forms.Button button_cancel;
        private System.Windows.Forms.Button btn_CheckInet;
        private System.Windows.Forms.ToolStripMenuItem cms1Search;
        private System.Windows.Forms.Button UndoButton;
        private System.Windows.Forms.Button RedoButton;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.Label label_central;
        private System.Windows.Forms.ToolStripMenuItem cms1Rename;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip3;
        private System.Windows.Forms.ToolStripMenuItem deleteEntryToolStripMenuItem;
        private System.Windows.Forms.Button button_download_start;
        private System.Windows.Forms.Button button_path;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl7;
        public System.Windows.Forms.Label label_progress;
        private System.Windows.Forms.Label label_counter;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip4;
        private System.Windows.Forms.ToolStripMenuItem cmsDeletePathItem;
        private System.Windows.Forms.ToolStripMenuItem cms1NewWIndow;
        private System.Windows.Forms.Button btn_clearfind;
        private System.Windows.Forms.Button btn_refind;
        private System.Windows.Forms.ContextMenuStrip cmLabelVlc;
        private System.Windows.Forms.ToolStripMenuItem cmLbl480;
        private System.Windows.Forms.ToolStripMenuItem cmLbl720;
        private System.Windows.Forms.ToolStripMenuItem cmLbl1080;
        private System.Windows.Forms.ToolStripMenuItem cms1KodiPlay;
        private System.Windows.Forms.ToolStripMenuItem cms1KodiQueue;
        private System.Windows.Forms.ContextMenuStrip cm5_Dup;
        private System.Windows.Forms.ToolStripComboBox cms5SearchCombo;
        private System.Windows.Forms.ToolStripMenuItem cms5SearchDupli;
        private System.Windows.Forms.ToolStripMenuItem cms1ExportList;
        private PathLabel plabel_Filename2;
        private System.Windows.Forms.ToolTip toolTip1;
        private RepeatingButton btnReverseRows;
        private RepeatingButton btnShuffleData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button_revert;
        private System.Windows.Forms.Label lblNoRows;
        private System.Windows.Forms.Panel panel_Find;
        private System.Windows.Forms.Button button_refind;
        private System.Windows.Forms.Button button_clearfind;
        private System.Windows.Forms.TextBox textBox_find;
        private PathLabel plabel_Filename;
        private System.Windows.Forms.ToolStripMenuItem cmLblAutoclose;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.Button btnSelectList;
        private System.Windows.Forms.ContextMenuStrip cm7_Open;
        private System.Windows.Forms.ToolStripMenuItem cm7Open;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem cm7Label1;
        private System.Windows.Forms.ToolStripMenuItem cm7Label2;
        private System.Windows.Forms.ToolStripMenuItem cm7Label3;
        private System.Windows.Forms.ToolStripMenuItem cm7Label4;
        private System.Windows.Forms.ToolStripMenuItem cm7Label5;
        private System.Windows.Forms.ToolStripMenuItem cm7Append;
        private System.Windows.Forms.ContextMenuStrip cm6_Save;
        private System.Windows.Forms.ToolStripMenuItem cm6Save;
        private System.Windows.Forms.ToolStripMenuItem cm6Saveas;
        private System.Windows.Forms.ToolStripMenuItem cm7NewWindow;
        private System.Windows.Forms.CheckBox checkBox_replaceIP;
        private System.Windows.Forms.ToolStripMenuItem cms5FileExt;
    }
}

