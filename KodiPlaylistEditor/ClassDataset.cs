﻿using PlaylistEditor.Properties;
using System.Drawing;
using System.Net.Http;

namespace PlaylistEditor
{
    public class ClassDataset
    {
        public static HttpClient _Client = new HttpClient();
        public static readonly string USERAGENT = "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36";

        //public static HttpClientHandler handler = new HttpClientHandler();
        //public static readonly HttpClient httpClient = new HttpClient(handler, true);

        public enum VideoType { Invalid, YT, YList, YMusic, BitC, Html, Rmbl, Vim, Daily, Lbry, nfs, table }

        public enum ClipMode { Copy, Cut }

        public enum RWMode { Write, Read, FirstRead }

        public enum NeedSave { Yes, No, Reset }

        public static Point MENUPOINT = new Point(9, 51);

        public string _vlcpath;
        public static string vlcpath { get; set; }

        public string _smppath;
        public static string smppath { get; set; }



        public string[] _videoext;
        public string[] VideoExt
        {
            get
            {
                return _videoext = new[] {
                    ".m4v", ".3g2", ".3gp",".nsv",".tp",".ts",".ty",".strm",".pls",".rm",
                    ".rmvb",".mpd",".m3u",".m3u8",".ifo",".mov",".qt",".divx",".xvid",".bivx",".vob",".nrg",".pva",
                    ".wmv",".asf",".asx",".ogm",".m2v",".avi",".dat",".mpg",".mpeg",".mp4",".mkv",".mk3d",".avc",
                    ".vp3",".svq3",".nuv",".viv",".dv",".fli",".flv",".001",".wpl",".vdr",".dvr-ms",".xsp",".mts",
                    ".m2t",".m2ts",".evo",".ogv",".sdp",".avs",".rec",".url",".pxml",".vc1",".h264",".rcv",".rss",
                    ".mpls",".webm",".bdmv",".wtv",".trp",".f4v"};

            }
        }


        public const string PLUG = "plugin://",
            YTURL = "https://www.youtube.com/watch?v=";

        public static readonly string YTPLUGIN = PLUG + Settings.Default.YTPLUGIN,
            VIPLUGIN = PLUG + Settings.Default.VIPLUGIN,
            LBRYPLUGIN = PLUG + Settings.Default.LBRYPLUGIN,
            RBLPLUGIN = PLUG + Settings.Default.RBLPLUGIN,
            DMPLUGIN1 = PLUG + Settings.Default.DMPLUGIN1,
            BCPLUGIN = PLUG + Settings.Default.BCPLUGIN,
            DMPLUGIN2 = "&mode=playVideo"; //;mode=playVideo&quot"; //plugin.video.dailymotion_com/?url=



        public class DupliRows
        {
            public int listNo { get; set; }
            public int row { get; set; }
            public int col { get; set; }
        }

        public class VideoData
        {
            public int id { get; set; } = 0;
            public string name { get; set; }
            public string link { get; set; }
            public VideoType videoType { get; set; }
            public int listNo { get; set; }

        }

        public class MRUItem
        {
            public int id { get; set; }
            public string Name { get; set; }
            public string File { get; set; }
        }

    }
}
